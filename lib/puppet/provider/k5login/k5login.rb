# Type for handling k5login files.  This differs from the version in Puppet
# proper by supporting purging and sorting the file.
require 'puppet/util'

Puppet::Type.type(:k5login).provide(:k5login) do
  @doc = "Manage the `.k5login` file for a user.  Specify the full path to
    the `.k5login` file as the name and an array of principals as the
    `principals` attribute."

  defaultfor :kernel => :linux

  # Does this file exist?
  def exists?
    File.exists?(@resource[:name])
  end

  # Create the file.
  def create
    write(@resource.should(:principals))
    should_mode = @resource.should(:mode)
    unless self.mode == should_mode
      self.mode = should_mode
    end
  end

  # Remove the file.
  def destroy
    File.unlink(@resource[:name])
  end

  # Return the principals.
  def principals(dummy_argument=:work_arround_for_ruby_GC_bug)
    if File.exists?(@resource[:name])
      princs = File.readlines(@resource[:name]).collect { |line|
        line.chomp
      }

      # If we aren't purging, ignore values we aren't trying to manage.  And
      # either way, return the array sorted
      if @resource[:purge] == :false
        princs.delete_if { |princ|
          ! @resource.should(:principals).include?(princ)
        }
      end
      princs.sort
    else
      :absent
    end
  end

  # Write the principals out to the k5login file.
  def principals=(value)
    write(value)
  end

  # Return the mode as an octal string, not as an integer.
  def mode
    "%o" % (File.stat(@resource[:name]).mode & 007777)
  end

  # Set the file mode, converting from a string to an integer.
  def mode=(value)
    File.chmod(Integer("0#{value}"), @resource[:name])
  end

  # If we are purging, just rewrite the entire file.  Otherwise, add in
  # the values that aren't in the current file.
  private
  def write(value)
    if @resource[:purge] == :true
      begin
        mode = Integer("0#{@resource[:mode]}")
        Puppet::Util.replace_file(@resource[:name], mode) do |f|
          f.puts value.join("\n")
        end
      rescue NoMethodError
        Puppet::Util.secure_open(@resource[:name], "w") do |f|
          f.puts value.join("\n")
        end
      end
    else
      principals = self.principals
      princs_missing = []
      value.each { |princ|
        if principals == :absent or ! principals.include?(princ)
          princs_missing.push(princ)
        end
      }
      File.open(@resource[:name], "a") do |f|
        f.puts princs_missing.join("\n")
      end
    end
    File.chmod(Integer("0#{@resource[:mode]}"), @resource[:name])
  end
end
