# @summary
#   Install and manage kerberos.
#
# Installs or removes kerberos client packages and configuration file.
# The content of the configuration file is defined in hiera.
#
# @example
#   include tcg_kerberos
#
# @param ensure
#   Install/remove kerberos client configuration and packages.
#
# @param manage_etc_krb5conf
#   Whether to manage /etc/krb5.conf or not.
#
# @param client
#   Whether to use MIT or Heimdal kerberos client.
#
# @param appdefaults
#   A hash containing the appdefaults section data for /etc/krb5.conf
#
# @param libdefaults
#   A hash containing the libdefaults section data for /etc/krb5.conf
#
# @param realms
#   A hash containing the realms section data for /etc/krb5.conf
#
# @param domain_realm
#   A hash containing the domain_realm section data for /etc/krb5.conf
#
# @param logging
#   A hash containing the logging section data for /etc/krb5.conf
#
class tcg_kerberos (
  Enum['present', 'absent', 'unmanaged'] $ensure = 'present',
  Boolean $manage_etc_krb5conf                   = true,
  Boolean $manage_packages                       = true,
  Enum['heimdal', 'mit'] $client                 = 'mit',
  Optional[Hash] $appdefaults                    = undef,
  Optional[Hash] $libdefaults                    = undef,
  Optional[Hash] $realms                         = undef,
  Optional[Hash] $domain_realm                   = undef,
  Optional[Hash] $logging                        = undef,
){
  if $manage_packages {
    $packages = {
      'heimdal' => { 'RedHat' => 'heimdal-workstation', 'Debian'  => 'heimdal-clients' },
      'mit'     => { 'RedHat' => 'krb5-workstation',    'Debian'  => 'krb5-user' }
    }

    package { $packages[$client][$facts['os']['family']]: ensure => $ensure }
  }

  if $manage_etc_krb5conf {
    file { '/etc/krb5.conf':
      ensure  => $ensure,
      content => template('tcg_kerberos/etc/krb5.conf.erb')
    }
  }
}
