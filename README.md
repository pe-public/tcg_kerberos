# tcg_kerberos

## Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with tcg_kerberos](#setup)
    * [Beginning with tcg_kerberos](#beginning-with-tcg_kerberos)
3. [Usage - Configuration options and additional functionality](#usage)

## Description

The module manages kerberos client packages and configuration file.

This is a simplified module in comparison with
[su_kerberos](https://code.stanford.edu/puppetpublic/su_kerberos)
working in conjunction with [krb5conf](https://code.stanford.edu/suitpuppet/krb5conf)
resource and is designed only to work with MIT and Heimdal clients
on Debian and Red Hat platforms. The advantage of this module is
it simplicity and flexibility allowing to granularly and easily control the
content of `/etc/krb5.conf` kerberos configuration file.

## Setup

### Beginning with tcg_kerberos

To use default Stanford kerberos client configuration, just include the
module in your code:

```
include tcg_kerberos
```

## Usage

The module has the following parameters:

* `ensure` - set to *present* to install kerberos support or *absent* to remove.
* `manage_etc_krb5conf` - if set to *false*, puppet does not manage the
  `/etc/krb5.conf` configuration file allowing to make manual edits.
* `manage_packages` - set to *false*, if you want to install custom set of
  packages.
* `client` - pick MIT or Heimdal kerberos client, MIT is default.
* `appdefaults` - a hash describing *appdefaults* section of `/etc/krb5.conf`.
* `libdefaults` - a hash describing *libdefaults* section of `/etc/krb5.conf`.
* `realms` - a hash describing *realms* section of `/etc/krb5.conf`.
* `domain_realm` - a hash describing *domain_realm* section of `/etc/krb5.conf`.
* `logging` - a hash describing *logging* section of `/etc/krb5.conf`.

Each section of the `krb5.conf` configuration file is controled by a
separate hash to simplify overriding. Each hash contains key/value pairs
correspodning to these in the resulting file. Nested hash configurations are
also supported to any level of nesting. Unlike a hash, kerberos configuration
may contain multiple values for the same parameter, like multiple `kdc`.
To provide multiple values for the same key, arrange them in an array.
For instance,

```yaml
tcg_kerberos::realms:
  stanford.edu:
    kdc:
      - krb5auth1.stanford.edu:88
      - krb5auth2.stanford.edu:88
      - krb5auth3.stanford.edu:88
    master_kdc: master-kdc.stanford.edu:88
    admin_server: krb5-admin.stanford.edu
    kpasswd_server: krb5-admin.stanford.edu
    default_domain: stanford.edu
    kadmind_port: 749
```

would produce the following in the `realms` section of the `krb5.conf`:

```
[realms]
    stanford.edu = {
        kdc            = krb5auth1.stanford.edu:88
        kdc            = krb5auth2.stanford.edu:88
        kdc            = krb5auth3.stanford.edu:88
        master_kdc     = master-kdc.stanford.edu:88
        admin_server   = krb5-admin.stanford.edu
        kpasswd_server = krb5-admin.stanford.edu
        default_domain = stanford.edu
        kadmind_port   = 749
    }
```
