# Changelog

All notable changes to this project will be documented in this file.

## Release 1.0.0

Initial release.

**Features**

* Only client kerberos configuration.
* Simple and easy.
* Allows granular control of `/etc/krb5.conf` content.
